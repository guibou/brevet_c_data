## AVION

01 À bord de l'avion.
	 - A La répartition de masse n'a pas d'importance pour le vol
	 - B La répartition de masse a une influence directe sur le centrage et la stabilité de l'avion


02 Pendant la montée de l'avion, on peut se déplacer à bord
	 - A Oui sans problème
	 - B Non car cela modifie le centrage de l'avion


03 À bord de l'avion et surtout au moment du décollage
	 - A On peut se déplacer à condition de respecter les limites de centrage
	 - B Il vaut mieux éviter de se déplacer


04 Lors du décollage, si les parachutistes se déplacent inconsidérément à bord de l'avion, celui-ci peut décrocher
	 - A Vrai
	 - B Faux


05 Durant les manœuvres de décollage et d'atterrissage, parler au pilote présente une gène, voire un risque
	 - A Vrai
	 - B Faux


06 Une vitesse de largage trop faible peut entraîner
	 - A Un meilleur confort pour la sortie
	 - B Un risque de décrochage
	 - C Un risque de percuter le plan fixe


07 Pour l'atterrissage et le décollage, la sortie des dispositifs hypersustentateurs (volets) est pour le pilote
	 - A Obligatoire quel que soit le cas
	 - B Interdite quel que soit le cas
	 - C Fonction de paramètres tels que vent, longueur de piste, etc.


08 À bord de l'avion, pendant la montée, vous remarquez un écoulement de liquide
	 - A Vous ne dites rien à personne
	 - B Vous sautez normalement et en arrivant au sol vous prévenez le directeur technique
	 - C Vous prévenez immédiatement le pilote


09 En montant à bord de l'avion, vous remarquez une arête coupante dans l'encadrement de porte
	 - A Vous ne dites rien et vous sautez en faisant attention en sortant
	 - B Vous sautez et vous prévenez le directeur technique une fois arrivé au sol
	 - C Vous prévenez immédiatement le pilote


10 En montant à bord de l'avion avec 7 parachutistes, vous remarquez que le trim du Pilatus est en position "plein cabré" avant le décollage
	 - A Vous ne dites rien car c'est une position de Trim normale au décollage en pleine charge pour un Pilatus
	 - B Vous ne dites rien car cette position n'a pas d'influence sur le décollage, quelque soit la charge et le centrage du Pilatus
	 - C Vous en informez le pilote dès l'embarquement à bord du Pilatus


11 La vitesse de largage d'un avion est définie par le manuel de vol
	 - A Vrai
	 - B Faux


12 Au moment de monter à bord de l'avion, vous remarquez une fuite de carburant venant d'un réservoir
	 - A À cause de la dilatation, cela est normal, vous ne dites rien
	 - B Cela est anormal vous prévenez immédiatement le pilote
	 - C Vous attendez d'avoir effectué le saut pour prévenir votre directeur technique


13 Les avions ayant une issue de sauts à l'arrière de la carlingue sont très sensibles aux problèmes de centrage
	 - A Vrai
	 - B Faux


14 Le centrage peut limiter le nombre de parachutistes pouvant effectuer une sortie de groupe
	 - A Vrai
	 - B Faux

15 Un nombre trop important de parachutistes effectuant une sortie de groupe sur un avion ayant une issue de sauts située à l'arrière peut induire
	 - A Uniquement un décrochage
	 - B Uniquement un départ en tonneaux
	 - C Uniquement une vrille à plat
	 - D Un des points précédents et éventuellement une combinaison des trois propositions précédentes


16 Au moment d'embarquer, vous remarquez d'importantes traînées d'huile sous la carlingue de l'avion
	 - A Cela est normal, c'est le graissage aérodynamique de la cellule de l'avion
	 - B Vous prévenez immédiatement le pilote
	 - C Vous ne dites rien, cela est le problème du mécanicien


17 La vitesse de largage est définie par
	 - A Les parachutistes en fonction du type de saut
	 - B Le pilote
	 - C Le constructeur et consignée dans le manuel de vol

18 Au moment d'embarquer, vous remarquez une crique (fissure) sur le plan fixe de l'avion
	 - A Vous ne dites rien, le pilote ayant effectué une visite prévol sait ce qu'il fait
	 - B Vous avertissez immédiatement le pilote
	 - C Vous attendez d'avoir effectué votre saut pour avertir quelqu'un de votre remarque


19 La vitesse de largage d'un Pilatus est d'environ
	 - A 70 Kts
	 - B 110 Kts
	 - C 45 Kts


20 La vitesse de largage d'un Cessna {206 ; 207) est d‘environ
	 - A 50 Kts
	 - B 80 Kts
	 - C 105 Kts


21 La vitesse de largage d'un gros porteur genre Hercules est d'environ
	 - A 70 Kts
	 - B 120 Kts
	 - C 200 Kts


22 Une vitesse de largage trop importante pour un avion léger de type Cessna peut entraîner une collision d'un chuteur avec le plan fixe
	 - A Vrai
	 - B Faux


## MATÉRIEL, ALTIMÈTRE et ALTISONS

23 Plus on monte en altitude.
	 - A Plus un altimètre est précis
	 - B Moins un altimètre est précis

24 Sur axe, à 4000 mètres, on peut observer une différence entre tous les altimètres d'un stick d'environ
	 - A Plus ou moins 12 m
	 - B Plus ou moins 40 m
	 - C Plus ou moins 200 m
	 - D Plus ou moins 500 m


25 Il n'est pas normal que certains altimètres se bloquent, en montée, aux alentours de 4800 m
	 - A Vrai
	 - B Faux


26 "Tapoter" au sol sur un altimètre pour faire se déplacer l'aiguille
	 - A N'a aucune conséquence
	 - B Est indispensable pour remettre l'aiguille à zéro
	 - C Augmente le jeu normal du fonctionnement et n'apporte qu'un vieillissement prématuré de l'appareil


27 Les altisons ont tous une alarme sonore de non fonctionnement ou de mauvais fonctionnement
	 - A Vrai
	 - B Faux


## DÉCLENCHEURS FXC 12000

28 Le FXC 12000
	 - A Déclenche à une hauteur fixe à partir d'une vitesse supérieure à 5 m/s
	 - B Déclenche à une hauteur que l'on règle avant chaque saut à partir d'une vitesse supérieure à 5 m/s
	 - C Déclenche à une hauteur fixe à partir d'une vitesse supérieure à 12 m/s
	 - D Déclenche à une hauteur que l'on règle avant chaque saut à partir d'une vitesse supérieure à 12 m/s


29 Sur un FXC 12000 réglé avant le saut, l'aiguille disparaît pendant la montée en avion
	 - A Il faut le signaler à un moniteur
	 - A Il est en panne, il ne faut pas sauter
	 - C C'est normal, l'aiguille disparaît à hauteur de réglage

30 Un déclencheur FXC 12000 dont l'aiguille de réglage est sur le 1 signifie
	 - A Réglage à 1000 pieds (soit sensiblement 300m)
	 - B Réglage à 100 m
	 - C Réglage à 1000 m

31 Le saut est annulé, l'avion redescend rapidement, les déclencheurs de sécurité de type FXC 12000 peuvent déclencher l'ouverture du secours dans l'avion.
	 - A Vrai
	 - B Faux


32 Le saut est annulé, l'avion redescend rapidement, les déclencheurs FXC 12000 réglés sur 1000 pieds peuvent déclencher à partir de 1200 m
	 - A Toujours vrai
	 - B Faux
	 - C Possible


33 Le saut est annulé, l'avion redescend rapidement, les déclencheurs FXC 12000 réglés sur 1000 pieds peuvent déclencher à partir de 400 m
	 - A Toujours vrai
	 - B Faux
	 - C Possible


34 Le saut est annulé, l'avion redescend rapidement. Les déclencheurs FXC 12000 réglés sur 1000 pieds peuvent déclencher à partir de 300 m
	 - A Vrai
	 - B Faux


35 Le saut est annulé, l'avion redescend à pleine charge, les déclencheurs FXC 12000 doivent être neutralisés en plaçant la molette sur OFF (couleur verte)
	 - A Vrai
	 - B Faux


36 À 3800 m, le saut est annulé, le pilote redescend, vous neutralisez les déclencheurs FXC 12000. À 3000 m, le pilote autorise le saut. Pouvez-vous remettre les déclencheurs FXC en fonction ?
	 - A Oui
	 - B Non


37 Un élève est tombé dans une grande flaque d'eau. Le déclencheur FXC 12000 a été immerge peu de temps
	 - A Il doit être déposé et révisé uniquement s'il n'est pas possible de le faire sécher
	 - B Il n'est pas nécessaire de le déposer et de le faire réviser
	 - C Il doit être déposé et révisé


38 Un déclencheur FXC 12000 doit être déposé et révisé à intervalle régulier
	 - A Tous les 6 mois
	 - B Tous les ans
	 - C Tous les 2 ans


39 Vous partez sauter sur un terrain dont l'altitude est 200 m plus haut que le lieu où vous décoller. Pour que les FXC déclenchent à 1000 pieds
	 - A Vous réglez sur 1700 pieds
	 - B Vous réglez sur 2300 pieds
	 - C Impossible de régler


40 Vous partez sauter sur un terrain dont l'altitude est 400 m plus bas que le lieu où vous décollez. Pour que les FXC déclenchent à 1000 pieds
	 - A Vous réglez sur 1700 pieds
	 - B Vous réglez sur 2300 pieds
	 - C Impossible de régler


## CYPRES I et II

41 Le CYPRES II
	 - A Ne craint absolument pas l'humidité
	 - B Peut être définitivement mis hors d'usage par une immersion


42 Un CYPRES affiche le code 0
	 - A Il faut changer les piles
	 - B Il faut effectuer le réglage
	 - C Le déclencheur est prêt à être utilisé


43 Si un CYPRES I a été immergé
	 - A Il faut le laver et le sécher avant une nouvelle utilisation
	 - B Aucun problème, le CYPRES est étanche
	 - C Il faut le renvoyer chez le constructeur


44 Au bout de combien de temps un CYPRES (I ou II) doit-il être révisé ?
	 - A Tous les ans
	 - B Il n'y a pas besoin de le réviser, il faut simplement changer les piles tous les deux ans
	 - C Il faut le réviser quand on change les piles
	 - D Il faut le réviser tous les quatre ans


45 Le CYPRES expert
	 - A Déclenche à une hauteur fixe à partir d'une vitesse supérieure à 13 m/s
	 - B Déclenche à une hauteur qu'il faut régler avant chaque saut à partir d'une vitesse supérieure à 13 m/s
	 - C Déclenche à une hauteur fixe à partir d'une vitesse supérieure à 35 m/s
	 - D Déclenche à une hauteur qu'il faut régler avant chaque saut à partir d'une vitesse supérieure à 35 m/s


46 Le CYPRES école
	 - A Déclenche à 300 m à partir d'une vitesse supérieure à 13 m/s et à 225 m à partir d'une vitesse supérieure à 35 m/s
	 - B Déclenche à 225 m à partir d'une vitesse supérieure à 13 m/s
	 - C Déclenche à 300 m à partir d'une vitesse supérieure à 35 m/s
	 - D Déclenche à une hauteur qu'il faut régler avant chaque saut à partir d'une vitesse supérieure à 35 m/s


47 Un déclencheur CYPRES confirmé, si la vitesse est suffisante, déclenche à une hauteur de
	 - A 175 m
	 - B 225 m
	 - C 315 m


48 En cas de descente rapide avec l'avion, un déclencheur CYPRES confirmé peut déclencher
	 - A Possible
	 - B Faux
	 - C Vrai


49 La vitesse de chute minimum nécessaire pour déclencher un CYPRES confirmé est supérieure ou égale à
	 - A 25 m/s
	 - B 30 m/s
	 - C 35 m/s


50 La vitesse de chute minimum nécessaire pour déclencher un CYPRES élève est supérieure ou égale à
	 - A 13 m/s à partir de 300 m
	 - B 33 m/s à partir de 225 m
	 - C 35 m/s à partir de 300 m


51 En cas d'action du CYPRES, il y a
	 - A Sectionnement du loop de secours
	 - B Action sur l'aiguille du secours
	 - C Libération de la voile principale


52 Les batteries d'un CYPRES I confirmé doivent être changées
	 - A Après chaque déclenchement
	 - B Tous les deux ans ou tous les 500 sauts
	 - C Uniquement lorsque le voltage des batteries tombe en dessous de 5,5 volts


53 Les batteries d'un CYPRES II doivent être changées
	 - A À chaque pliage périodique
	 - B Après chaque déclenchement
	 - C Tous les deux ans ou tous les 500 sauts
	 - D Tous les quatre ans


54 À 3800 m, le saut est annulé, vous désarmez les CYPRES à 3000m. Le pilote annonce que le saut peu avoir lieu. Pouvez-vous réarmer les CYPRES sans risques pour le saut ?
	 - A Oui
	 - B Non


55 À bord de l'avion, vous remarquez un CYPRES élève dont le boîtier de contrôle indique 8993.
	 - A Vous le laissez sauter
	 - B Vous ne le laissez pas sauter


56 Le boîtier de contrôle d'un CYPRES en fonction indique
	 - A 0
	 - B Rien d'écrit
	 - C Jump


57 Le CYPRES doit être réarmé tous les matins même s'il est encore en marche
	 - A Vrai
	 - B Faux


58 Le CYPRES s'éteint automatiquement 14 heures après sa mise en route
	 - A Oui
	 - B Sauf si l'on ressaute entre temps
	 - C Sauf si les batteries sont faibles


59 Vous décollez pour sauter sur une zone située 200 m plus haut et distante de 50 Km ; vous réglez les CYPRES sur
	 - A 200 flèche vers le haut
	 - B 200 flèche vers le bas
	 - C Réglage impossible


60 En cas d'évacuation à 300 m pendant la montée, le CYPRES (confirmé et élève) peut déclencher
	 - A Vrai
	 - B Faux car il ne s'active qu'à partir de 450 m
	 - C Réglage impossible

## VIGIL

61 Le VIGIL possède trois modes d'utilisation
	 - A Vrai
	 - B Ça dépend des modèles
	 - C Faux


62 La révision du VIGIL doit être effectuée
	 - A Tous les quatre ans
	 - B Uniquement dans le cas d'un message d'erreur
	 - C Tous les 700 sauts


63 Le changement des piles sur un VIGIL s'effectue
	 - A Jamais
	 - B Tous les deux ans
	 - C Tous les quatre ans, tous les 700 sauts ou présence du message "bat low"


64 Pour les versions confirmées, les hauteurs de déclenchement du VIGIL et du CYPRES sont sensiblement identiques
	 - A Vrai
	 - B Faux


65 La vitesse de déclenchement du VIGIL en mode Student est de
	 - A 12 m/s
	 - B 20 m/s
	 - C 35 m/s


## PARACHUTES

66 Un parachute doit être stocké
	 - A Au sec et à l'abri du soleil
	 - B Les conditions de stockage n'ont pas d'importance


67 L'exposition au soleil
	 - A Est un facteur de vieillissement des matières textiles
	 - B N'induit pas de vieillissement anormal

68 Les loops de fermeture
	 - A Doivent être très courts pour éviter une ouverture intempestive
	 - B Doivent être relâchés pour faciliter l'ouverture
	 - C Doivent être tendus normalement pour éviter à la fois les blocages et les ouvertures intempestives


69 Qu'elle peut être la conséquence de l'utilisation d'un loop trop long ?
	 - A Une ouverture intempestive
	 - B Un retard à l'ouverture


70 Qu'elle peut être la conséquence de l'utilisation d'un loop trop court ?
	 - A Une ouverture intempestive
	 - B Un blocage du système d'ouverture (poignée, hand deploy, pull out...)


71 Qu'est—ce qu'un système de rétraction ?
	 - A C'est l'action de tirer sur les élévateurs
	 - B C'est le système qui permet de déventer l'extracteur après l'ouverture


72 Le parachute de secours doit être replié
	 - A Par une personne qualifiée
	 - B Par n'importe quel parachutiste confirmé


73 Lors du repliage du parachute de secours
	 - A Le plieur qualifié ne contrôle la voilure que si elle a été ouverte en vol
	 - B Le plieur Qualifié contrôle la voilure dans tous les cas mais ne contrôle rien d'autre
	 - C Le plieur qualifié fait un contrôle détaillé de l'ensemble de l'équipement


74 Quel est l'effort maximal admissible pour la poignée d'ouverture du conteneur secours ?
	 - A 5 daN
	 - B 9 daN
	 - C 12 daN
	 - D 15 daN


75 Un contrôle détaillé et un entretien du système de libération de la voilure principale doivent être effectués régulièrement par une personne qualifiée
	 - A Oui
	 - B Non


76 Quels sont les contrôles et l'entretien à réaliser sur le système de libération trois anneaux ?
	 - A Aucun contrôle particulier, le système étant visible en permanence
	 - B Manipuler les sangles et nettoyer les gaines de câbles et les joncs de la poignée
	 - C Nettoyer les gaines de câbles et les joncs avec de l'essence avion uniquement

77 Sur une sangle principale de harnais, une dégradation de 5 mm peut entraîner une perte de résistance de 50 %
	 - A Vrai
	 - B Faux


78 Rallonger exagérément la longueur de la bouclette de verrouillage (loop) d'un parachute pour permettre une fermeture aisée génère des risques d'ouverture intempestive.
	 - A Vrai
	 - B Faux


79 Pour une bouclette de verrouillage (loop), utiliser une suspente d'un diamètre trop important peut générer une non ouverture du conteneur secours
	 - A Vrai
	 - B Faux


80 Des expositions prolongées et répétées au soleil peuvent réduire la durée de vie d'une voilure de manière importante
	 - A Vrai
	 - B Faux


81 Quel phénomène, généré par le soleil, induit un vieillissement prématuré des voilures
	 - A Les rayons gamma
	 - B Les rayons électromagnétiques
	 - C Les rayons ultra violets


82 Vous remarquez un début de décousure entre deux sangles d'un sac harnais
	 - A Cela n'est pas gênant si elle fait moins de 8 mm
	 - B Cela n'est pas gênant si elle fait moins de 5 mm
	 - C Vous ne laissez pas sauter le parachute et vous consultez un moniteur ou un plieur réparateur


83 Une déchirure dans un sac de déploiement
	 - A Ne nécessite pas de réparation si elle fait moins de 2 cm
	 - B Ne nécessite pas de réparation si elle fait moins de 5 cm
	 - C Ne nécessite pas de réparation si elle fait moins de 8 cm
	 - D Nécessite un contrôle par une personne qualifiée quelque-soit sa longueur

84 Une suspente présentant une amorce de rupture, même légère, peut voir sa résistance diminuée de façon importante
	 - A Vrai
	 - B Faux


85 Une déchirure sur une voilure principale
	 - A Ne nécessite pas de réparation si elle fait moins de 2 cm
	 - B Ne nécessite pas de réparation si elle fait moins de 5 cm 
	 - C Ne nécessite pas de réparation si elle fait moins de 8 cm
	 - D Nécessite un contrôle par une personne qualifiée quelque-soit sa longueur


## ENCADREMENT À BORD

86 Vous êtes responsable d'avion, durant la montée en altitude, vers 1200 m, le moteur de l'avion s'arrête.
	 - A Vous ouvrez la porte et vous sautez en enjoignant aux autres de vous suivre
	 - B Vous ouvrez la porte et faites évacuer.
	 - C Vous attendez les consignes du pilote pour ouvrir la porte et procéder à l'évacuation


87 Pour un saut à 4000 m avec la moitié de l'avion composé d'élèves. Lors de l'équipement le vent est de 3 m/s. Au décollage le vent est de 7 m/s : à 2500 m le pilote vous annonce 9 m/s avec rafales à 11 m/s.
	 - A Il faut annuler le largage, demander au pilote de redescendre et désarmer les déclencheurs
	 - B Vous larguez les élèves tout de suite
	 - C Vous poursuivez la montée et larguez à 4000 m


88 L'avion est bloqué à 800 m par des nuages, vous êtes avec des élèves.
	 - A Vous larguez en demandant aux élèves d'ouvrir tout de suite
	 - B Vous redescendez avec l'avion avec les élèves
	 - C Les élèves redescendent avec l'avion et vous sautez


89 Au moment du largage.
	 - A Vous demandez au pilote de réduire la puissance.
	 - B Il doit être prévenu seulement si ce sont les derniers parachutistes qui partent en premier.
	 - C Vous pouvez larguer à pleine puissance.


90 À 1500 m de hauteur, le pilote annonce que le largage est annulé et qu'il va redescendre et se poser à pleine charge
	 - A Il faut désarmer les déclencheurs de sécurité ou demander au pilote de respecter une vitesse de descente en fonction du type de déclencheur
	 - B Il faut faire cela uniquement si l'avion vole porte ouverte
	 - C Il faut faire cela uniquement en dessous de 500 m.


91 À 1800 m de hauteur, le pilote réduit la puissance et se met à descendre
	 - A Ne sachant exactement ce qui se passe, vous sautez immédiatement
	 - B Il faut couper les déclencheurs de sécurité ou demander au pilote de respecter une vitesse de descente en fonction du type de déclencheur
	 - C Vous désarmez les déclencheurs uniquement en dessous de 500 m.


92 Un élève vous dit dans l'avion qu'il ne veut pas sauter parce qu'il se sent mal
	 - A Vous essayez de le convaincre qu'il peut sauter quand même
	 - B Il doit redescendre avec l'avion
	 - C Vous le poussez dehors


93 L'avion est bloqué à 800 m par des nuages, vous êtes avec des élèves.
	 - A Vous larguez en demandant aux élèves d'être stable avant d'ouvrir
	 - B Vous redescendez avec l'avion avec les élèves
	 - C Vous demandez au pilote de monter de 50 m dans la couche pour pouvoir larguer et sauter à la hauteur réglementaire


94 Au moment du largage
	 - A Il ne faut pas sauter sans l'accord du pilote
	 - B C'est sans importance


95 Pour embarquer dans l'avion,
	 - A Il ne faut jamais passer devant l'hélice.
	 - B Il ne faut pas passer derrière l'avion.
	 - C C'est sans importance.


96 À bord de l'avion, deux parachutistes titulaires du brevet A vous demandent l'autorisation de faire un VR ensemble
	 - A Vous les autorisez en leur donnant des conseils
	 - B Vous les autorisez sans leur donner de conseil, cela est le travail d'un initiateur VR.
	 - C Vous ne les laissez pas faire


97 À 3000 m, vous remarquez que l'altimètre d'un élève indique 3800 m
	 - A Vous lui demandez de le recaler à 3000 m
	 - B Vous ne dites rien pour ne pas l'effrayer
	 - C Cela est normal si la température est supérieure à la normale
	 - D Vous lui demandez de ne pas sauter


98 À bord de l'avion, vous remarquez lors de la vérification que le système trois anneaux du parachute d'un élève est mal monté
	 - A La probabilité d'une procédure de secours pour un élève étant très faible, vous ne dites rien pour ne pas l'inquiéter
	 - B Vous démontez et remontez l'élévateur en demandant au pilote de refaire un tour
	 - C L'élève ne doit pas sauter


99 À bord de l'avion, vous remarquez lors de la vérification, que le mousqueton débrayable du LOR équipant le parachute d'un élève est cassé
	 - A Vous laissez sauter l'élève sans le prévenir pour ne pas l'inquiéter
	 - B L'élève ne doit pas sauter
	 - C Vous prévenez l'élève en lui recommandant d'ouvrir à la bonne hauteur


100 À 4000 m, si l'avion est encore en pente de montée avec une assiette de 10° et que vous ne corrigez pas l'angle de visée, vous commettez une erreur de largage de
	 - A Environ 100 m
	 - B Environ 300 m
	 - C Environ 700 m
	 - D Environ 1300 m


## RÉGLEMENTATION GÉNÉRALE

101 En VFR, le pilote traverse une couche nuageuse pour monter à 3800 m, personne ne voit le sol lors du largage et pendant les sauts
	 - A Cela est une infraction
	 - B Cela ne constitue pas une infraction
	 - C Si le pilote est titulaire d'une licence professionnelle cela est possible


102 En VFR, la couche de nuages est à 3000 m, vous demandez au pilote de monter dans la couche nuageuse jusqu'à 3800 m et de larguer au GPS
	 - A Le pilote est en infraction
	 - B Le pilote n'est pas en infraction
	 - C Si le pilote est titulaire d'une licence privée, cela est possible


103 Le responsable à bord est
	 - A Toujours le pilote commandant de bord
	 - B Le largueur
	 - C Le commandant de bord sauf pendant les phases de largage où c'est le largueur


104 Un avion qui largue au GPS dans une couche nuageuse est en infraction. En cas d'accident, l'assurance peut ne pas fonctionner
	 - A Vrai
	 - B Faux


105 Un avion largueur
	 - A N'est pas soumis aux règles générales d'utilisation des aéronefs
	 - B Est soumis aux mêmes règles d'utilisation que tous les aéronefs, complétées par des règles spécifiques

106 Pour organiser une manifestation aérienne, il faut
	 - A Déposer une demande à la préfecture et aux services de la DGAC.
	 - B Déposer une demande à la mairie uniquement
	 - C Déposer une demande à la FFP uniquement


107 L'organisateur d'une manifestation aérienne
	 - A Doit souscrire une assurance particulière pour l'évènement
	 - B N'est pas tenu de souscrire une assurance particulière


108 Les manifestations aériennes
	 - A Sont toutes soumises aux mêmes règles
	 - B Sont soumises à des règles différentes selon qu'elles sont de petite, moyenne ou de grande importances.


109 Lors d'une manifestation aérienne
	 - A La responsabilité et la coordination des vols incombent à l'organisateur de la manifestation
	 - B La responsabilité et la coordination des vols incombent au directeur des vols désigné


110 Lors d'un saut de démonstration
	 - A C'est à l'organisateur de prévoir une zone d'atterrissage dégagée de tous publics
	 - B C'est à la gendarmerie ou à la police de faire dégager la zone
	 - C C'est le public qui doit dégager la zone quand les parachutistes sont en approche


111 Pour organiser une séance de sauts en parachute
	 - A Il faut l'autorisation du propriétaire du terrain et celle des services de l'aviation civile
	 - B Il faut uniquement l'autorisation du maire
	 - C Il faut l'autorisation du propriétaire du terrain


112 En VFR, la traversée d'une couche nuageuse, même mince, est interdite
	 - A Vrai
	 - B Faux


113 Le pilote titulaire d'une licence professionnelle ou privée en conditions de vol VMC peut se voir retirer sa licence temporairement ou à vie s'il traverse une couche nuageuse ou s'il pénètre dans un nuage
	 - A Vrai
	 - B Faux


114 L'installation d'un CYPRES sur un parachute peut être fait
	 - A Par tous les parachutistes confirmés, à condition de suivre les indications du manuel constructeur
	 - B Uniquement par une personne qualifiée


115 Pour le parachutisme, l'aéronef doit avoir une assurance en responsabilité civile
	 - A Vrai
	 - B Faux

116 Pour un aéronef, le C. D. N. veut dire.
	 - A Commandes de nuit
	 - B Centrale de navigation
	 - C Certificat de navigabilité


## RÉGLEMENTATION FÉDÉRALE

117 Si un parachutiste arrive sur votre gauche avec une trajectoire convergente
	 - A Il a la priorité
	 - B Il n'a pas la priorité
	 - C Il n'y a pas de règle particulière


118 Si un parachutiste arrive face à vous
	 - A Chacun doit dégager sur sa droite
	 - B Chacun doit dégager sur sa gauche
	 - C Il n'y a pas de règle particulière


119 Est-ce que l'utilisation d'un pull out nécessite une qualification spéciale
	 - A Oui
	 - B Non


120 Le parachute utilisé pour une qualification hand deploy ou pull out doit-il être équipé d'un déclencheur de sécurité ?
	 - A Oui
	 - B Non


121 Le pliage du parachute de secours doit être fait au minimum
	 - A Tous les 3 mois ou après une ouverture en vol
	 - B Tous les 6 mois ou après une ouverture en vol
	 - C Tous les ans ou après une ouverture en vol
	 - D Uniquement après chaque ouverture en vol


122 Un parachutiste titulaire du brevet C peut-il sauter n'importe où ?
	 - A Oui, le brevet C le permet
	 - B Oui, à condition d'avoir l'autorisation du propriétaire du terrain
	 - C Oui à condition d'avoir l'autorisation du propriétaire du terrain et celle des services de l'aviation civile


123 Quel est le brevet qui donne l'autorisation d'effectuer des sauts de nuit ?
	 - A Le brevet A
	 - B Le brevet B
	 - C Le brevet C


124 Quel est le brevet qui donne la possibilité de participer à des compétitions ?
	 - A Le brevet A
	 - B Le brevet B dans la spécialité correspondante
	 - C Le brevet C


125 Pour effectuer des sauts de nuit
	 - A L'emport d'une lampe est obligatoire
	 - B L'emport d'une lampe n'est pas obligatoire


126 La durée de la validité de pliage d'un parachute de secours est de
	 - A 3 mois
	 - B 6 mois
	 - C 12 mois
	 - D 24 mois


127 La vitesse de vent maximale pour des confirmés est de
	 - A 12 m/s
	 - B 11 m/s
	 - C 9 m/s
	 - D 7 m=s


128 La vitesse de vent maximum pour des élèves débutants est de
	 - A 5 m/s
	 - B 6 m/s
	 - C 7 m/s
	 - D 8 m/s


129 Une assurance responsabilité civile est obligatoire pour les parachutistes qui pratiquent dans les structures de la F.F.P.
	 - A Vrai
	 - B Faux
	 - C Uniquement pour les compétiteurs


130 La hauteur d'ouverture minimale imposée par la réglementation fédérale pour les parachutistes confirmés est de
	 - A 650 m
	 - B 850 m
	 - C 950 m
	 - D 1000 m


131 Quels sont les critères réglementaires de validité du brevet C
	 - A Avoir la licence assurance de l'année en cours et avoir effectué 10 sauts dans les 6 derniers mois
	 - B Avoir 20 sauts dans les six derniers mois
	 - C Avoir un carnet de sauts visé par un directeur technique


132 Quel est le document attestant l'affiliation à la FFP
	 - A L'assurance responsabilité civile
	 - B Le carnet de sauts et un des brevets Fédéraux
	 - C La licence assurance de l'année en cours


133 Les documents de sauts qu'un parachutiste doit présenter pour sauter dans les structures fédérales sont
	 - A Le carnet de sauts uniquement
	 - B Les documents parachute uniquement
	 - C La licence assurance uniquement
	 - D Tous ces documents


134 L'assurance en responsabilité civile
	 - A Garantit le remboursement des frais médicaux et chirurgicaux de l'assuré
	 - B Garantit la réparation des dommages causés aux tiers


135 L'assurance individuelle
	 - A Garantit le remboursement des frais médicaux et chirurgicaux de l'assuré
	 - B Garantit la réparation des dommages causés aux tiers


136 Un pratiquant doit pouvoir présenter aux responsables d'une école
	 - A Son carnet de sauts uniquement
	 - B Son carnet de sauts et les documents du parachute uniquement
	 - C Son carnet de sauts, un justificatif d'assurance et les documents du parachute


137 Pour organiser une séance de sauts en parachute, il faut :
	 - A L'autorisation du propriétaire du terrain, l'autorisation de la DGAC et un NOTAM.
	 - B L'autorisation du propriétaire du terrain uniquement.
	 - C L'autorisation de la DGAC et un NOTAM uniquement.


138 Le titulaire d'un brevet C peut effectuer des sauts spéciaux
	 - A Vrai
	 - B Faux


139 Le titulaire d'un brevet B peut effectuer des sauts spéciaux
	 - A Vrai
	 - B Faux


140 Le titulaire d'un brevet C peut effectuer des sauts hors aérodrome
	 - A Vrai
	 - B Faux


141 Le titulaire d'un brevet B peut effectuer des sauts en manifestation aérienne
	 - A Toujours vrai
	 - B Faux
	 - C Vrai s'il totalise plus de 250 sauts et 10 sauts minimum dans les trois derniers mois


## AÉRODYNAMIQUE ET MÉCANIQUE DE VOL

142 En faisant un virage rapide parachute ouvert
	 - A La vitesse verticale augmente beaucoup
	 - B La vitesse verticale augmente peu
	 - C La vitesse verticale diminue


143 Pour augmenter la pénétration dans l'air par vent fort
	 - A Il faut faire une légère traction sur les élévateurs avant
	 - B Il faut faire une légère traction sur les élévateurs arrière
	 - C On ne peut pas améliorer la pénétration par vent fort


144 Qu'appelle-t-on finesse ?
	 - A L'épaisseur du profil d'une aile
	 - B Le rapport entre la surface et la masse du parachutiste équipé
	 - C Le rapport entre la vitesse horizontale et la vitesse verticale de la voilure


145 Qu'appelle-t-on l'angle de plané ?
	 - A L'angle entre la trajectoire et l'horizontale
	 - B L'angle entre l'aile et l'axe du cône de suspension
	 - C L'angle entre la trajectoire et la corde de profil de l'aile


146 La charge alaire
	 - A Est le rapport entre la masse du parachutiste équipé et la surface de voile
	 - B Est le poids de la voilure
	 - C Est le poids du parachutiste "en l'air"


147 Le décrochage
	 - A Survient uniquement suite à une action sur les commandes de manœuvre
	 - B Peut survenir indépendamment de l'action quand on passe une zone de fortes turbulences


148 Avec une voilure
	 - A La charge alaire est un paramètre important
	 - B La charge alaire n'a pas d'importance


149 Si votre voilure décroche
	 - A Il faut agir sur les commandes par tractions répétées
	 - B Il faut attendre que la voilure se remette en pression toute seule
	 - C Il faut relâcher doucement les commandes de manœuvre


150 Une finesse de 2,5 signifie
	 - A Que pour une perte de hauteur de 1000 m, la distance horizontale parcourue sera de 2500 m
	 - B Que pour une perte de hauteur de 2500 m, la distance horizontale parcourue sera de 1000 m


151 Lors d'un virage, la vitesse verticale
	 - A Diminue
	 - B Augmente
	 - C Ne change pas


152 La finesse maximum sur une voilure de type aile est obtenue en pilotant
	 - A Bras haut
	 - B À environ 25 à 30 % de freinage
	 - C Près du décrochage


153 Sur une voilure "rapide" type 110 pieds carrés, la vitesse en sortie de virage très engagé peut dépasser les 100 km/h
	 - A Vrai
	 - B Faux


154 Le dessus d'une voilure de type aile s'appelle
	 - A L'intrados
	 - B L'extrados
	 - C L'écope


155 Le dessous d'une voilure de type aile s'appelle
	 - A L'intrados
	 - B L'extrados
	 - C L'écope


156 Lors du "décrochage", la vitesse verticale d'une voilure de type aile
	 - A Ne change pas
	 - B Augmente un petit peu
	 - C Augmente beaucoup


157 Une voilure de type aile peut "décrocher" même si l'on n'a pas dépassé le point de décrochage avec les commandes.
	 - A Vrai
	 - B Faux


158 Sur une voilure rapide, type 110 ou 120 pieds carrés, après un virage engagé sur 180°, l'abaissement avant le retour à un vol stabilisé peut être supérieur à
	 - A 1000 m
	 - B 300 m
	 - C 60 m


159 Sur une voilure rapide, type 110 ou 120 pieds carrés, après un virage engagé sur 180°, l'abaissement avant le retour à un vol stabilisé peut être supérieur à
	 - A 5 m
	 - B 13 m
	 - C 60 m


160 Si on augmente la masse sous un parachute, les vitesses verticale et horizontale
	 - A Augmentent
	 - B Diminuent
	 - C Ne changent pas


161 L'avant d'une voilure de type aile s'appelle
	 - A Le saumon
	 - B Le bord d'attaque
	 - C Le bord de fuite


162 L'arrière d'une voilure de type aile s'appelle
	 - A Le bord de fuite
	 - B Le bord d'attaque
	 - C L'intrados


163 Lors d'un décrochage, la vitesse horizontale
	 - A Augmente
	 - B Ne change pas
	 - C Diminue


164 Après un décrochage, il faut plusieurs secondes pour que la voilure reprenne sa ligne de vol
	 - A Vrai
	 - B Faux


165 Après un décrochage, la perte de hauteur avant le retour à un vol normal peut être supérieure à
	 - A 20 m
	 - B 100 m
	 - C Pas de perte de hauteur


166 La vitesse de descente d'une voilure pour une masse et un pourcentage de frein donné
	 - A Est supérieure face au vent
	 - B Est identique quel que soit le vent
	 - C Est supérieure dans le vent
	 - D Dépend du gradient turbulo-laminaire de vent


167 S'il y a du vent, une voilure de type aile se met naturellement dans le vent
	 - A Vrai
	 - B Faux
	 - C Uniquement à partir de 9.81 m/s.


168 S'il v a du vent, une voilure de type aile se met naturellement contre le vent
	 - A Vrai
	 - B Faux
	 - C Uniquement à partir de 9.81 m/s.


## TECHNIQUES DE LARGAGE

169 La vitesse de vol et la trajectoire de l'avion par rapport au sol
	 - A Ne donne pas d'indications sur le vent en altitude
	 - B Donne des indications sur le vent en altitude


170 L'avion vole à 70 Kts face au vent au moment du largage. Un parachutiste de 80 kg qui ouvre à 900 m et une parachutiste de 50 kg qui ouvre a 1200 m partent l'un après l'autre. Ils utilisent tous les deux une voilure école.
	 - A Lui part en premier, elle part 5" après
	 - B Lui part en premier mais il faut espacer davantage les départs
	 - C Elle part en premier et lui 5" après

171 En chute libre
	 - A On ne subit aucune dérive
	 - B On subit une dérive égale au temps de chute multipliée par la vitesse du vent
	 - C On subit une dérive égale à la distance parcourue multipliée par la vitesse du vent

172 Parachute ouvert
	 - A On ne subit aucune dérive
	 - B On subit une dérive égale au temps de descente multipliée par la vitesse du vent
	 - C On subit une dérive égale à la distance parcourue multipliée par la vitesse du vent


173 Au moment du largage, il faut
	 - A Se contenter de vérifier que l'on saute au-dessus du terrain et assurer l'espacement entre les départs
	 - B Contrôler l'axe, le point de largage et assurer l'espacement entre les départs


174 Au moment du largage, s'assurer que l'espace aérien est dégagé
	 - A Est le problème du pilote uniquement
	 - B Est le problème du directeur technique uniquement
	 - C Est le problème du pilote, du directeur technique et du largueur


175 Ceux qui chutent le plus vite, par exemple le "free-fly"
	 - A Subissent une dérive plus importante que ceux qui chutent à plat
	 - B Subissent une dérive moins importante que ceux qui chutent à plat

176 Ceux qui ouvrent haut
	 - A Subissent une dérive plus grande que ceux qui ouvrent plus bas
	 - B Subissent une dérive moins grande que ceux qui ouvrent plus bas

177 De quoi dépend la distance de séparation entre deux parachutistes qui ne sautent pas ensemble
	 - A De la vitesse de l'avion uniquement
	 - B Du temps laissé entre deux départs uniquement
	 - C De la vitesse de l'avion et du temps entre deux départs


178 Qu'est-ce que la projection ?
	 - A C'est la distance horizontale parcourue pendant les dix premières secondes de chute
	 - B C'est l'impulsion donnée en sortie d'avion
	 - C C'est le temps mis pour atteindre la vitesse maximale de chute


179 Qu'est-ce que la dérive totale due au vent au cours d'un saut?
	 - A C'est la dérive en chute + la dérive parachute ouvert.
	 - B C'est un saut au cours duquel on prend la position de dérive pendant toute la chute
	 - C C'est un saut avec ouverture instantanée pour parcourir la plus grande distance horizontale


180 Pour larguer des élèves vent arrière, il faut.
	 - A Larguer un peu plus près de la cible que vent de face
	 - B Anticiper le départ des premiers
	 - C Aucune différence avec vent de face


181 Si au moment du largage, l'avion est en pente de montée, que faut-il faire ?
	 - A Allonger un peu le largage car on risque de partir trop tôt
	 - B Anticiper un peu le largage car on risque de partir trop tard


182 Si au moment du largage, l'avion est en pente de descente, que faut-il faire ?
	 - A Allonger un peu le largage car on risque de partir trop tôt
	 - B Anticiper un peu le largage car on risque de partir trop tard


183 Dans quel ordre larguez-vous un élève qui ouvre à 1500 m, un élève qui ouvre à 1200 m et vous-même
	 - A L'élève qui ouvre à 1500 m, celui qui ouvre à 1200 m, puis vous
	 - B L'élève qui ouvre à 1200 m, celui qui ouvre à 1500 m, puis vous
	 - C Vous, l'élève qui ouvre à 1500 m puis celui qui ouvre à 1200 m
	 - D Vous, l'élève qui ouvre à 1200 m puis celui qui ouvre à 1500 m


184 Larguer un élève pour une première chute assis derrière un élève en chute stable présente des risques de collision
	 - A Vrai
	 - B Faux


185 Larguer dans le même passage, l'un derrière l'autre deux élèves pour leur premier exercice dérive présente des risques de collision
	 - A Vrai
	 - B Faux


186 Larguer un "free flyer" derrière un saut de vol relatif présente des risques de collision
	 - A Vrai
	 - B Faux


187 Sur un avion de type Pilatus, à 3300 m, le temps entre deux départs est de
	 - A 1,5 seconde
	 - B 3 à 4 secondes
	 - C 7 à 9 secondes
	 - D 15 à 20 secondes


188 Sur un avion de type Pilatus, 5 départs successifs, en respectant les espacements recommandés impliquent une distance de largage de
	 - A Environ 300 m
	 - B Environ 500 m
	 - C Environ 1150 m
	 - D Environ 2200 m


189 La météorologie vous donne un vent de 35 Kts de 1000 à 4000 mètres. La dérive en chute de 4000 à 1000 m pour un parachutiste en chute à plat sera de
	 - A Environ 300 m
	 - B Environ 600 m
	 - C Environ 1100 m


190 Pour un largage sans vent avec un avion de type Pilatus, l'espacement entre chaque départ doit être de
	 - A 5 à 6 secondes
	 - B 8 a 10 secondes
	 - C Supérieur à 10 secondes


191 L'avion avance lentement par rapport au sol
	 - A Il vole face à un vent fort. Il faut augmenter le temps entre les départs
	 - B Il vole face à un vent Fort. Il faut diminuer le temps entre les départs
	 - C Il vole face à un vent fort. Je conserve l'espacement standard entre les départs, le vent en altitude n'ayant que peu d'influence


## PHYSIOlOGIE

192 Un élève vous dit dans l'avion qu'il a mal aux oreilles et qu'il n'a pas très envie de sauter
	 - A Vous le rassurez pour qu'il saute quand même
	 - B Vous lui dites de ne pas sauter


193 Avec l'altitude, les problèmes liés à l'hypoxie sont dus à
	 - A Une diminution de la pression partielle d'oxygène
	 - B Une augmentation de la pression partielle d'oxygène


194 Est-il important d'être en bonne condition physique pour sauter en parachute
	 - A Uniquement pour les compétiteurs
	 - B Non car le saut ne requiert pas beaucoup d'efforts
	 - C Oui car elle atténue les conséquences de micro-traumatismes répétés


195 Avant de débuter une journée de sauts
	 - A Il faut s'alimenter
	 - B Mieux vaut ne rien manger


196 L'absorption d'alcool pendant une journée de sauts
	 - A Est sans importance tant que l'on reste dans des limites raisonnables
	 - B Est à proscrire absolument
	 - C Donne un peu de courage


197 Comment appelle-t-on le phénomène physiologique dû à une insuffisance en oxygène ?
	 - A L'hyperventilation
	 - B L'hypoglycémie
	 - C L'hypoxie


198 Quelle est la partie du corps qui, souffrant du manque d'oxygène, est la plus problématique pour le parachutisme ?
	 - A Le cerveau
	 - B L'estomac
	 - C Les oreilles


199 À 6000 mètres, ne pas avoir d'oxygène à bord présente un risque de troubles physiologiques importants pouvant aller jusqu'à la perte de connaissance
	 - A Vrai
	 - B Faux


200 En cas d'hypoxie, le froid est un facteur aggravant vis a vis des problèmes rencontrés
	 - A Vrai
	 - B Faux, les deux sont complètement indépendants

201 Les problèmes d'hypoxie peuvent apparaître à partir de 3500 m.
	 - A Vrai
	 - B Faux


202 Les problème liés à l'hypoxie sont dépendants du temps passé en altitude
	 - A Vrai
	 - B Faux


## CONNAISSANCE GÉNÉRALES

203 À quel moment doit—on s'équiper pour aller sauter
	 - A Suffisamment tôt pour s'équiper calmement et se faire vérifier
	 - B Le plus tard possible pour ne pas se fatiguer en restant équipé longtemps


204 Pour embarquer dans un hélicoptère léger
	 - A Il faut aborder l'appareil par l'avant
	 - B Il faut aborder l'appareil par l'arrière
	 - C C'est sans importance


205 En montant dans l'avion
	 - A On risque d'accrocher quelque chose sur son équipement si l'on ne fait pas attention
	 - B Ce risque est négligeable

206 Quand l'avion prend l'axe de largage
	 - A Il faut contrôler quelques points essentiels avant de sauter
	 - B Il est inutile de contrôler son équipement

207 En sortie d'avion
	 - A Il faut veiller à ne rien accrocher sur son équipement
	 - B Il ne faut plus s'occuper de problèmes matériels
	 - C La vitesse est moins élevée


208 Pour sauter la première fois d'un avion gros porteur
	 - A je m'informe des consignes particulières
	 - B Inutile, je suivrai les autres


209 En chute, la vitesse moyenne à plat face au sol
	 - A Est proche de 150 km/h
	 - B Est proche de 200 km/h
	 - C Est proche de 250 km/h


210 En chute, tête en bas ou en chute debout, la vitesse
	 - A Est sensiblement la même qu'à plat face sol
	 - B Ne dépasse pas 250 km/h
	 - C Peut atteindre 300 Km/h


211 Lors d'un saut de groupe
	 - A Il faut toujours assurer la sécurité en chute pour éviter tout risque de collision
	 - B Les risques de collision sont faibles car tout le monde chute à la même vitesse


212 Lors d'un saut de groupe
	 - A Avant d'ouvrir, il suffit de dériver longtemps pour éviter tout risque de collision
	 - B Il faut dériver en contrôlant sa trajectoire et garder le contact visuel sur les autres parachutistes pour s'assurer que la séparation est suffisante


213 Si vous n'avez pas sauté depuis plusieurs mois, vous programmez
	 - A N'importe quel type de saut
	 - B Un saut de reprise, sans exercice particulier, avec un matériel que vous connaissez et en adaptant la hauteur d'ouverture


214 En faisant des virages rapides et enchaînés en dessous de 500 m
	 - A Il n'y a pas de risques particuliers
	 - B On risque de faire fonctionner le déclencheur de sécurité et d'entrer en collision avec d'autres parachutistes


215 Si l'on fait un virage prononcé avec une voilure rapide type 120 pieds carrés près du sol
	 - A Il n'y a aucun problème car l'on peut interrompre le virage à tout moment
	 - B Cela est dangereux et réclame une très grande maîtrise de cette technique car cette manœuvre induit une augmentation de vitesse très importante.


216 Si l'on doit se poser sous voilure en altitude
	 - A La vitesse est sensiblement la même qu'au niveau de la mer
	 - B La vitesse est plus élevée
	 - C La vitesse est moins élevée


217 Les conditions aérologiques en montagne sont
	 - A Plus difficiles qu'en plaine, surtout en été
	 - B Plus difficiles qu'en plaine, surtout en hiver
	 - C Identiques à celles que l'on rencontre en plaine quelle que soit la saison


218 L'atterrissage dans une pente prononcée, par temps calme sur un grand terrain où rien n'indique le vent se fait de préférence
	 - A Face à la pente
	 - B Dans le sens de la pente
	 - C En travers de la pente


219 Parachute ouvert, vous constatez que vous ne pourrez pas rejoindre la zone de sauts. Il faut
	 - A Se rapprocher au maximum au plus prés de la zone, quel que soit le terrain survolé
	 - B Ne pas survoler d'obstacles, surtout a basse hauteur


220 Piste 27 en service signifie
	 - A Que l'avion va décoller face au nord
	 - B Que l'avion va décoller face à l'est
	 - C Que l'avion va décoller face au sud
	 - D Que l'avion va décoller face à l'ouest


221 Piste 09 en service signifie
	 - A Que l'avion va décoller face au nord
	 - B Que l'avion va décoller face à l'est
	 - C Que l'avion va décoller face au sud
	 - D Que l'avion va décoller face à l'ouest


222 Piste 18 en service signifie
	 - A Que l'avion va décoller face au nord
	 - B Que l'avion va décoller face à l'est
	 - C Que l'avion va décoller face au sud
	 - D Que l'avion va décoller face à l'ouest


223 Piste 36 en service signifie
	 - A Que l'avion va décoller face au nord
	 - B Que l'avion va décoller face à l'est
	 - C Que l'avion va décoller face au sud
	 - D Que l'avion va décoller face à l'ouest


224 Parachute ouvert, vous constatez que vous ne pourrez pas rejoindre la zone de sauts, il faut
	 - A Repérer dés que possible les obstacles et les zones dégagées
	 - B Attendre d'être près du sol pour bien voir les obstacles


225 En finale d'atterrissage, hors zone, vous vous rapprochez dangereusement d'une ligne électrique face à vous
	 - A Il faut l'éviter à tout prix vous changez de trajectoire quitte à vous poser vent de travers
	 - B Vous conservez votre trajectoire en freinant au maximum et en espérant que ça va passer


226 Lors d'un atterrissage hors zone, l'objectif prioritaire
	 - A Se poser face au vent 
	 - B Se poser hors obstacles avec une trajectoire dégagée


227 En montant dans l'avion
	 - A Si vous remarquez quelque chose d'anormal, il faut le signaler au pilote
	 - B Il ne faut rien lui dire pour ne pas le perturber


228 Quand l'avion prend l'axe de largage
	 - A Il faut contrôler son équipement pour éviter une ouverture intempestive en chute ou à la mise en place
	 - B Il est inutile de contrôler son équipement


229 En sortie d'avion en position flotteur
	 - A Il est possible de se tenir à n'importe quelle partie de la carlingue
	 - B Des équipements spécifiques doivent être en place pour se tenir


230 Vous sortez troisième du passage
	 - A Vous laissez environ 7 secondes, vous surveillez le précédent et vous contrôlez la zone sur laquelle vous partez
	 - B Seul le temps de séparation compte


231 Vous partez premier du passage, le pilote donne l'autorisation de sortie
	 - A Vous ouvrez la porte et vous sautez instantanément. Le pilote sait ce qu'il fait
	 - B Vous ouvrez la porte, vous contrôlez la zone où vous allez partir puis vous sautez


232 Contrôler ou faire contrôler son équipement avant la sortie d'avion est
	 - A Inutile puisque déjà fait au sol
	 - B Indispensable quel que soit le niveau du parachutiste
	 - C Cela est valable juste pour les élèves


233 Vous sortez deuxième d'un passage. Le précèdent est en dérive et remonte l'axe
	 - A Vous sortez en respectant le temps pour ne pas mettre les suivants hors zone
	 - B Vous sortez en respectant le temps pour ne pas mettre les suivants hors zone en pensant à surveiller en chute le précédent
	 - C Vous rallongez le largage en surveillant le précédent, et si nécessaire, vous redemandez un autre passage


234 Avant de s'équiper
	 - A Il faut contrôler les points de sécurité sur l'équipement
	 - B C'est inutile


235 Après une procédure de secours
	 - A Il est possible d'essayer de récupérer en vol la voilure libérée
	 - B Il est dangereux d'essayer de récupérer en vol la voilure libérée


236 Après une procédure de secours
	 - A Il est possible d'essayer de récupérer en vol le sac de déploiement du secours libéré
	 - B Il est dangereux d'essayer de récupérer en vol le sac de déploiement du secours libéré


237 En chutant tête en bas, avec un départ à 4000 m. Si la verticalité n'est pas parfaitement maîtrisée, le parachutiste peut avoir un déplacement horizontal supérieur à 1000 m.
	 - A Vrai
	 - B Faux


238 En "free-fly", si la position n'est pas parfaitement maîtrisée. On peut atteindre temporairement des vitesses horizontales supérieures à 100 km/h ?
	 - A Vrai
	 - B Faux


239 Que signifient les deux chiffres placés à l'entrée d'une piste ?
	 - A La longueur de la piste
	 - B L'orientation de la piste
	 - C L'immatriculation OACI de l'aérodrome


## ALTIMÉTRIE

240 Pour convertir des m/s en pieds/minute, on utilise la règle suivante :
	 - A 1000 pieds/minute = 30 m/s
	 - B 1000 pieds/minute = 3 m/s
	 - C 1000 pieds/minute = 5 m/s


241 Dire qu'un mètre est égal à trois pieds est
	 - A Juste
	 - B Très approximatif


242 Un mètre est égal à
	 - A 3 pieds
	 - B 3,2808 pieds soit environ 3,3 pieds


243 En "atmosphère" standard
	 - A La pression au niveau de mer est de 1013.25 hPa. La température au niveau de la mer est de 15°. La température diminue de 6.5° tous les 1000 mètres
	 - B La pression au niveau de mer est de 1025.13 hPa. La température au niveau de la mer est de 15°. La température diminue de 6.5° tous les 1000 mètres
	 - C La pression au niveau de mer est de 1013.25 hPa. La température au niveau de la mer est de 15°. La température diminue de 8.5° tous les 1000 mètres

244 Un pied est égal à :
	 - A 30 cm
	 - B 30,48 cm
	 - C 33 cm


## CALAGE ALTIMETRIQUE

245 L'altimètre fournit au parachutiste des informations d'altitude en mesurant des différences
	 - A De pression
	 - B De température
	 - C De masse volumique


246 Un niveau de vol
	 - A Est l'indication donnée par l'altimètre quand il est réglé à zéro au sol
	 - B Est l'indication donnée par l'altimètre quand il est réglé au QNH au sol
	 - C Est l'indication donnée par l'altimètre quand il est réglé à 1013.25 hPa au sol


247 Voler au niveau 115 signifie
	 - A Qu'un altimètre calé à 1013.25 hPa indique 11500 m
	 - B Qu'un altimètre calé à 1013.25 hPa indique 11500 pieds
	 - C Que l'on vole à une hauteur réelle de 11500 pieds


248 L'altitude
	 - A Est la distance sur un axe vertical entre un point et le niveau de la mer
	 - B Est la distance sur un axe vertical entre un point et le sol
	 - C Est la distance sur un axe vertical entre un point et la surface isobare 1013.25 hPa


249 La hauteur
	 - A Est la distance sur un axe vertical entre un point et le niveau de la mer
	 - B Est la distance sur un axe vertical entre un point et le sol
	 - C Est la distance 5m un axe vertical entre un point et la surface isobare 1013.25 hPa


250 Une hauteur de 3000 pieds en atmosphère standard est approximativement
	 - A 900 m
	 - B 1000 m
	 - C 1100 m


251 Le pied est une unité de mesure qui est égale à :
	 - A 30.48 cm
	 - B 33 cm
	 - C 0.30 cm


252 Quand on est au FL 145
	 - A On lit 14500 pieds sur un altimètre calé au 1013
	 - B On lit 14500 pieds sur un altimètre calé à zéro au sol
	 - C On lit 14500 pieds sur un altimètre calé au niveau de la mer


253 Un parachutiste qui met son altimètre à zéro avant le saut
	 - A Le cale au QNH
	 - B Le cale au QFE
	 - C Le cale au 1013


## Problèmes pratiques

254 En vol, à 4000 m de hauteur, vous constatez que votre altimètre indique 200 m de moins que celui de votre voisin
	 - A Vous corrigez le réglage
	 - B Vous ne corrigez pas le réglage


255 Pour effectuer un saut sur une zone située 200 mètres plus bas que la zone de décollage, je règle mon altimètre à :
	 - A + 200 m
	 - B - 200 m
	 - C Je le cale à Zéro


256 Le calage au 1013 ou calage standard est utilisé pour
	 - A Survoler les reliefs
	 - B Éviter les collisions entre avions
	 - C N'est pas utilisé en aviation

257 Le soir au sol, votre altimètre indique 0, le matin, il indique +100 m. Qu'en déduisez-vous ?
	 - A Une dépression arrive ou la pression baisse
	 - B On va vers une situation anticyclonique


258 Le soir au sol, votre altimètre indique 0, le matin, il indique -100 m. Qu'en déduisezsvous ?
	 - A Une dépression arrive
	 - B On va vers une situation anticyclonique ou la pression augmente


259 Si vous devez sauter sur un terrain situé plus haut que le terrain de décollage
	 - A Sur le terrain de décollage, vous affichez en moins la différence de hauteur
	 - B Sur le terrain de décollage, vous affichez en plus la différence de hauteur


260 Si vous devez sauter sur un terrain situé plus bas que le terrain de décollage
	 - A Sur le terrain de décollage, vous affichez en moins la différence de hauteur
	 - B Sur le terrain de décollage, vous affichez en plus la différence de hauteur


261 Pourquoi régle-t-on un altimètre au QNH lors d'un déplacement en avion ?
	 - A Pour connaître son altitude par rapport au relief
	 - B Pour connaître la hauteur par rapport au sol
	 - C Pour utiliser les niveaux de vol


262 Quel rapprochement peut-on faire entre un baromètre et un altimètre de saut ?
	 - A Aucun, ils n'ont pas la même fonction
	 - B Les deux instruments mesurent des variations de pression


263 Pour sauter sur une zone plus élevée de 200 m que la zone de décollage, vous devez
	 - A Régler l'altimètre avant d'embarquer à - 200 m
	 - B Régler l'altimètre avant d'embarquer a + 200 m
	 - C Régler l'altimètre pendant la montée en avion


264 Pour sauter sur une zone plus basse de 200 m que la zone de décollage, vous devez
	 - A Régler l'altimètre avant d'embarquer à - 200 m
	 - B Régler d'altimètre avant d'embarquer à + 200 m
	 - C Régler l'altimètre pendant la montée en avion


## MÉTÉOROLOGIE Pression, température et humidité

265 En atmosphère standard.
	 - A La température décroît de 6.5° C tous les 1000 m
	 - B La température décroît de 10° C tous les 1000 m


266 En atmosphère standard, si la température est de 20° C au sol, à 4000 m elle est de
	 - A -20° C
	 - B +13.5° C
	 - C -6° C


267 En atmosphère standard, si la température est de 10° C au sol, à 3000 m elle est de
	 - A 0° C
	 - B -9.5° C
	 - C +6.5° C


268 En atmosphère standard, si la température est de 0° C au sol, à 4000 m elle est de
	 - A -54° C
	 - B -26° C
	 - C -10° C


269 Les lignes d'égale pression représentées sur les cartes météos s'appellent
	 - A Les lignes isobares
	 - B Les isothermes
	 - C Les isohypses


270 Une zone de hautes pressions généralisées s'appelle
	 - A Un anticyclone
	 - B Une dépression
	 - C Un thalweg
	 - D Une dorsale


271 Une zone de basses pressions généralisées s'appelle
	 - A Un anticyclone
	 - B Une dépression
	 - C Un thalweg
	 - D Une dorsale


272 Une Crête de haute pression est
	 - A Un anticyclone
	 - B Une dépression
	 - C Un thalweg
	 - D Une dorsale


273 Une vallée de basse pression est
	 - A Un anticyclone
	 - B Une dépression
	 - C Un thalweg
	 - D Une dorsale


274 Quand on monte en altitude, la pression atmosphérique
	 - A Augmente
	 - B Diminue
	 - C Augmente ou diminue suivant la situation météo


275 La variation de pression entre le sol et 500 m est en moyenne de
	 - A 1 hPa tous les 8.5 m
	 - B 1 hPa tous les 11 m
	 - C 1 hPa tous les 17 m


276 Quand on monte en altitude en dessous de 500 m, la pression atmosphérique
	 - A Augmente de 1 hPa tous les 3.5 m
	 - B Diminue de 1 hPa tous les 8.5 m
	 - C Diminue de 1 hPa tous les 11 m


277 Un nuage est constitué
	 - A De vapeur d'eau uniquement 
	 - B D'eau à l'état liquide ou solide


278 On parle de brouillard
	 - A Quand la visibilité est inférieure à 100 m
	 - B Quand la visibilité est inférieure à 1 km
	 - C Quand la visibilité est réduite mais supérieure à 1 km


279 On parle de brume
	 - A Quand la visibilité est inférieure à 100 m
	 - B Quand la visibilité est inférieure à 1 km
	 - C Quand la visibilité est réduite mais supérieure à 1 km


280 À 5600 m d'altitude, la pression partielle d'oxygène
	 - A N'a pas changé
	 - B A diminué de 30%
	 - C A diminué de moitié


## BRISES

281 En bord de mer, par ciel couvert sans vent météo
	 - A Il y aura probablement une brise soufflant de la terre vers la mer
	 - B Il y aura probablement une brise soufflant de la mer vers la terre
	 - C Il n'y aura probablement pas de brise


282 En bord de mer en été en milieu d'après-midi, par temps ensoleillé sans vent météo
	 - A Il y aura probablement une brise soufflant de la terre vers la mer
	 - B Il y aura probablement une brise soufflant de la mer vers la terre
	 - C Il n'y aura probablement pas de brise


283 Dans une vallée étroite, en milieu d'après midi, par temps ensoleillé, il y aura probablement
	 - A Une forte brise de vallée, montante, qui risque d'empêcher les sauts
	 - B Une forte brise de vallée, montante, mais il est toujours possible de sauter
	 - C Une forte brise de vallée descendante


284 Sur une pente, par temps ensoleillé, on rencontre en milieu de journée
	 - A Des brises montantes
	 - B Des brises descendantes


285 Les brises de vallée soufflent dans la même direction toute la journée
	 - A Vrai
	 - B Faux


## VENT

286 Le vent est dû
	 - A Aux différences de pression atmosphérique et aux différences de température.
	 - B Aux marées
	 - C Essentiellement aux phénomènes orageux


287 Un vent du sud-ouest
	 - A Souffle en direction du sud-ouest
	 - B Souffle en provenance du sud—ouest
	 - C Souffle du sud au sol et de l'ouest en altitude


288 Le vent du nord
	 - A Souffle en direction du nord
	 - B Souffle en provenance du nord


289 Un vent de 10 Kts souffle à environ
	 - A 5 m/s
	 - B 10 m/s
	 - C 20 m/s


290 Un vent de 10 Kts souffle à environ
	 - A 10 km/h
	 - B 18 km/h
	 - C 36 km/h


291 Un vent de 10 m/s souffle à environ
	 - A 10 km/h
	 - B 18 km/h
	 - C 36 km/h


292 Un vent de 18 Kts souffle à environ
	 - A 18 m/s
	 - B 9 m/s
	 - C 5 m/s


293 Un vent de 7 m/s souffle à environ
	 - A 18 km/h
	 - B 25 km/h
	 - C 36 km/h


294 Un vent de 5 m/s souffle à environ
	 - A 5 km/h
	 - B 10 km/h
	 - C 18 km/h


295 Quelle est la vitesse du vent quand il souffle à 20 Kts ?
	 - A 5 m/s
	 - B 10 m/s
	 - C 18 m/s


296 La météo annonce un vent du 270. Cela veut dire qu'il vient ?
	 - A Du nord
	 - B De l'ouest
	 - C Du sud ouest


297 Qu'appelle—t-on inversion de vent ?
	 - A Le vent change de direction en altitude par rapport au vent au sol
	 - B C'est l'ordre de saut des parachutistes qui est inversé à cause du vent
	 - C Le vent souffle de façon irrégulière dans toutes les directions


298 Qu'appelle—t-on cisaillement de vent ?
	 - A Un vent suffisamment fort pour couper la cime des arbres
	 - B La zone de séparation entre deux vents de sens contraire


299 Dans l'hémisphère nord, le vent
	 - A Tourne autour des anticyclones dans le sens des aiguilles d'une montre et autour des dépressions en sens inverse
	 - B Tourne autour des dépressions dans le sens des aiguilles d'une montre et autour des anticyclones en sens inverse


## TURBULENCES

300 Avec du vent, le risque de turbulences est maximum
	 - A En plaine dans une zone dégagée
	 - B Sur un relief accidenté


301 Par fort vent, derrière un immeuble
	 - A Il y a des turbulences
	 - B Il n'y a pas de turbulences


302 Par fort vent derrière une haie d'arbres
	 - A Il y a des turbulences
	 - B Il n'y a pas de turbulences


303 Derrière un obstacle, les turbulences sont maximales
	 - A Quand le vent souffle perpendiculairement à l'obstacle
	 - B Quand le vent souffle parallèlement à l'obstacle


304 On risque de rencontrer des turbulences
	 - A En présence de stratus
	 - B En présence de cumulus


305 Un risque surtout de rencontrer des turbulences
	 - A Par vent fort
	 - B Par vent faible


306 On risque surtout de rencontrer des turbulences
	 - A Par ciel couvert
	 - B Par temps chaud et ensoleillé


307 Un risque surtout de rencontrer des turbulences
	 - A Au-dessus des surfaces chaudes
	 - B Au-dessus des surfaces froides
	 - C La température du sol n'a pas d'influence sur les turbulences

308 Les turbulences derrière une haie d'arbres ou un bâtiment de même hauteur, par vent de 5 m/s
	 - A Sont dangereuses jusqu'à plus de 50 m.
	 - B Sont dangereuses juste derrière sur quelques mètres
	 - C Ne sont pas dangereuses


309 Les vents rabattants en montagne
	 - A Sont seulement dangereux pour les avions
	 - B Ne sont pas dangereux, ils nous font dévier en plaine
	 - C Sont dangereux et risquent de nous plaquer contre le relief


310 En été par temps chaud et en présence de cumulus bien développés
	 - A Il y a probablement des turbulences
	 - B Il n'y a pas de turbulences


311 L'aérologie en montagne
	 - A Est souvent plus turbulente qu'en plaine
	 - B Est bien moins turbulente qu'en plaine


312 Par temps chaud, les turbulences sont plus importantes
	 - A Au-dessus des hangars et des parkings goudronnés
	 - B Au-dessus du terrain en herbe


## NUAGE

313 Les cumulus sont des nuages
	 - A Isolés et de forme bien marqués
	 - B En voile ou en nappe continue avec des contours flous
	 - C En filaments


314 Les nuages de type "cumulo" se forment
	 - A Dans des conditions d'instabilité verticale
	 - B Dans des conditions de stabilité verticale


315 Les stratus sont des nuages
	 - A Isolés et de forme bien marqués
	 - B En voile ou en nappe continue avec des contours flous
	 - C En filaments


316 Les nuages de type "strato" se forment
	 - A Dans des conditions d'instabilité verticale
	 - B Dans des conditions de stabilité verticale


317 Les cirrus sont des nuages
	 - A Isolés et de forme bien marqués
	 - B En voile ou en nappe continue avec des contours flous
	 - C En filaments


318 Le nimbostratus est un nuage
	 - A De beau temps
	 - B Épais associé à des précipitations durables


319 La présence d'une couche nuageuse en altitude (cirrocumulus ou cirrostratus)
	 - A Indique que le mauvais temps est passé
	 - B Annonce l'arrivée d'une perturbation


320 Les strato-cumulus sont des nuages
	 - A D'altitude
	 - B De l'étage moyen
	 - C Bas


321 Quel est le nuage le plus dangereux pour le parachutisme ?
	 - A Le cumulus de beau temps
	 - B Le cirro-stratus
	 - C Le cumulonimbus


## ORAGE

322 Lequel de ces nuages est un nuage d'orage ?
	 - A Le strato-cumulus
	 - B Le cumulonimbus
	 - C L'altocumulus


323 Voler près d'un cumulonimbus
	 - A N'est jamais dangereux tant que l'orage n'a pas éclaté
	 - B Peut se faire tout de suite après le moment le plus fort de l'orage
	 - C Est très dangereux


324 Un nuage d'orage
	 - A Évolue toujours très lentement
	 - B Peut évoluer rapidement


325 À l'approche d'un orage, il faut cesser les sauts
	 - A Dès que l'on a un doute sur la situation
	 - B Uniquement si le vent au sol dépasse la limite autorisée


326 À proximité d'un cumulonimbus
	 - A Il y a toujours de fortes turbulences
	 - B Il n'y a pas de turbulences


327 En situation orageuse, on rencontre
	 - A Des vents faibles
	 - B Des vents forts et réguliers
	 - C Des vents forts et en rafales qui peuvent atteindre des vitesses très élevées


328 L'orage est un phénomène
	 - A Très dangereux pour toutes les activités aéronautiques
	 - B Dangereux pour le parachutisme mais pas pour les avions


329 Un nuage d'orage
	 - A Se déplace toujours dans le sens du vent dominant
	 - B Se déplace parfois en sens contraire au vent dominant

330 Un cumulonimbus
	 - A Dépasse rarement 5000 m d'altitude à son sommet
	 - B Peut dépasser 5000 m mais jamais 10000 m
	 - C Peut dépasser 10000 m d'altitude


331 Le vent donne-t-il une indication sur l'évolution d'un cumulonimbus
	 - A Oui
	 - B Non


332 En avion
	 - A Il est possible de traverser un cumulonimbus à condition de ne pas y rester trop longtemps
	 - B C'est possible uniquement avec un bimoteur
	 - C Il ne faut jamais entrer dans un cumulonimbus


333 Une station météo proche du terrain passe un avis de tempête
	 - A Il faut arrêter immédiatement la séance de sauts
	 - B On peut continuer à sauter encore quelques temps en surveillant le vent, tant qu'il ne force pas
	 - C Il faut arrêter la séance quand le vent atteint la limite autorisée


334 Au passage d'un cumulonimbus, on peut s'attendre
	 - A À un renforcement soudain du vent faisant suite à un changement de direction
	 - B À un renforcement soudain du vent sans changement de direction


335 Sous un cumulonimbus, les précipitations sont souvent
	 - A Inexistantes
	 - B Faibles
	 - C Violentes


## SAUT SPÉCIAUX

336 Si l'on saute d'hélicoptère léger
	 - A Il n'y a pas de problème particulier
	 - B Il faut faire attention car la vitesse de largage est souvent inférieure à celle d'un avion ce qui oblige à espacer les départs
	 - C Il faut faire attention car la vitesse de largage est souvent supérieure à celle d'un avion ce qui oblige à moins espacer les départs


337 Il est possible de faire des sauts de nuit, en VFR de nuit, à la limite d'une couche nuageuse soudée
	 - A Oui
	 - B Non


338 Pour effectuer un saut de nuit, il faut
	 - A Une lampe et un altimètre fluorescent
	 - B Une lampe seulement
	 - C Un altimètre seulement


339 Lors d'un saut à 4500 m si l'on doit attendre 15 minutes sur axe de largage
	 - A Cela ne pose pas de problèmes particuliers
	 - B Les problèmes physiologiques dus à l'altitude sont beaucoup plus conséquents que si l'on n'a pas d'attente


340 Lors d'un saut à 4500 m, l'oxygène à bord
	 - A Est obligatoire parce que la réglementation le demande
	 - B N'est pas nécessaire
	 - C Est physiologiquement indispensable et réglementairement obligatoire


341 Lors d'un saut à 6000 m, la prise d'oxygène à partir de 4000 m est ?
	 - A Indispensable quel que soit le niveau des pratiquants
	 - B Obligatoire uniquement pour les brevets B
	 - C Nécessaire uniquement pour ceux qui en ressentent le besoin


342 Le contrôle des équipements avant la sortie a 6000 m doit être particulièrement rigoureux
	 - A Pour minimiser les risques d'ouverture en altitude
	 - B Pour ne pas risquer une ouverture à bord et payer un saut non fait
	 - C Cela n'est pas nécessaire


343 Le temps de chute (à plat) pour un départ à 6000 m est d'environ
	 - A 01 mn et 10 sec.
	 - B 01 mn et 40 sec.
	 - C 02 mn et 30 sec.


344 Si pour un saut à 6000 m, le vent pendant la phase de chute est de 10 m/s, la dérive en chute sera de
	 - A Environ 400 m.
	 - B Environ 1000 m.
	 - C Environ 1500 m.


345 Si pour un saut à 6000 m, le vent pendant la phase de chute est de 40 Kts, la dérive en chute sera de
	 - A Environ 1200 m.
	 - B Environ 2000 m.
	 - C Environ 5000 m.


346 Au cours d'un saut à 6000 m
	 - A Il faut surveiller sa chute pour éventuellement ouvrir un peu plus haut si une erreur de largage a été commise
	 - B Ne nécessite pas de surveillance en chute


347 Pour sauter d'un ULM
	 - A Il faut le brevet C
	 - B Il faut le brevet A
	 - C Il faut le brevet B


348 Pour sauter la première fois d'un ULM
	 - A Vous contrôlez que le pilote est titulaire d'une DNC
	 - B Cela ne concerne que les pilotes avion


349 Pour sauter la première fois d'un ULM
	 - A Vous voyez et répétez, avec le pilote, avant de décoller, la procédure pour sauter de la machine
	 - B Cela n'est pas nécessaire si vous êtes titulaire d'un brevet C


350 Pour sauter d'un ULM hors zone de sauts habituelle
	 - A Un NOTAM est obligatoire
	 - B Cela n'est pas nécessaire


351 Pour sauter de ballon à air chaud
	 - A Un NOTAM n'est pas nécessaire car on ne sait jamais où l'on saute
	 - B Un NOTAM est indispensable


352 Pour un saut de ballon à air chaud. Si le ballon s'est éloigné conséquemment de la zone de sauts prévue à cause du vent
	 - A Le saut est possible
	 - B Le saut doit être annulé


353 Pour un saut d'un ULM, la hauteur d'ouverture est
	 - A 1000 m
	 - B 850 m
	 - C 500 m, parce que ces appareils ne montent pas très haut
